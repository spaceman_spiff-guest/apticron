#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: apticron 1.1.13\n"
"Report-Msgid-Bugs-To: apticron@packages.debian.org\n"
"POT-Creation-Date: 2009-08-14 21:50-0300\n"
"PO-Revision-Date: 2006-08-12 15:00+0200\n"
"Last-Translator: Johannes Starosta <feedback-an-johannes@arcor.de>\n"
"Language-Team:German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "E-Mail address(es) to notify:"
msgstr "Zu benachrichtigende E-Mail-Adresse(n):"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Specify e-mail addresses, space separated, that should be notified of "
"pending updates."
msgstr ""
" Geben Sie die E-Mail-Adressen der Benutzer an, die von bevorstehenden "
"Updates informiert werden sollen. Trennen Sie mehrere E-Mail-Adressen mit "
"einen Leerzeichen."
